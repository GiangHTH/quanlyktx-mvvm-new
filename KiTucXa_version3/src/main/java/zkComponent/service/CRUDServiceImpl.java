/**
 * 
 */
package zkComponent.service;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zkComponent.dao.CRUDDao;
import zkComponent.model.PhongTro;
import zkComponent.model.SinhVien;

/**
 * @author PC
 *
 */
@Service
public class CRUDServiceImpl implements CRUDService {
	@Autowired
	CRUDDao cruddao;
	
	@Override
	@Transactional
	public <T> List<T> getAll(Class<T> klass) {
		return cruddao.getAll(klass);
	}

	@Override
	@Transactional
	public <T> void save(T klass) {
		cruddao.save(klass);
	}

	@Override
	public <T> void delete(T klass) {
		cruddao.delete(klass);
	}

	@Override
	@Transactional
	public <T> void update(T klass) {
		cruddao.update(klass);
	}

	@Override
	public List<SinhVien> searchSV(String keyword) {
		List<SinhVien> listSV = new LinkedList<SinhVien>();
		if (keyword == null || "".equals(keyword)) {
			listSV = getAll(SinhVien.class);
		} else {
			for (SinhVien sv : getAll(SinhVien.class)) {
				if (sv.getTen().toLowerCase().contains(keyword.toLowerCase())
						|| sv.getEmail().toLowerCase().contains(keyword.toLowerCase())
						|| sv.getSdt().toLowerCase().contains(keyword.toLowerCase())) {
					listSV.add(sv);
				}
			}
		}
		return listSV;
	}

	@Override
	public List<PhongTro> searchPT(String keyword) {
		List<PhongTro> listPT = new LinkedList<PhongTro>();
		if (keyword == null || "".equals(keyword)) {
			listPT = getAll(PhongTro.class);
		} else {
			for (PhongTro pt : getAll(PhongTro.class)) {
				if (pt.getTen().toLowerCase().contains(keyword.toLowerCase())) {
					listPT.add(pt);
				}
			}
		}
		return listPT;
	}
}
