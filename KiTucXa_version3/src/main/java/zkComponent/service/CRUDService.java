/**
 * 
 */
package zkComponent.service;

import java.util.List;

import zkComponent.model.PhongTro;
import zkComponent.model.SinhVien;

/**
 * @author PC
 *
 */
public interface CRUDService {
	<T> List<T> getAll(Class<T> klass);
	<T> void save(T klass);
	<T> void delete(T klass);
	<T> void update(T klass);
	public List<SinhVien> searchSV(String keyword);
	public List<PhongTro> searchPT(String keyword);
}
