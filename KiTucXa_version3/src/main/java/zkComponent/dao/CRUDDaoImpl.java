/**
 * 
 */
package zkComponent.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author PC
 *
 */
@Repository
public class CRUDDaoImpl implements CRUDDao {

	@Autowired
	private SessionFactory sessionFactory;

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getAll(Class<T> klass) {
		return getCurrentSession().createQuery("from " + klass.getName()+" where trangthai = 'apdung'").list();
	}

	@Override
	public <T> void save(T klass) {
		getCurrentSession().save(klass);
	}

	@Override
	public <T> void delete(T klass) {
		getCurrentSession().delete(klass);
	}

	@Override
	public <T> void update(T klass) {
		getCurrentSession().update(klass);
	}

}
