package zkComponent.dao;

import java.util.List;

public interface CRUDDao {
	<T> List<T> getAll(Class<T> klass);
	<T> void save(T klass);
	<T> void delete(T klass);
	<T> void update(T klass);
}
