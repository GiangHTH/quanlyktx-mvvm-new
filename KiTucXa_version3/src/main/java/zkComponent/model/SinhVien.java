/**
 * 
 */
package zkComponent.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author PC
 *
 */
@Entity
@Table(name = "sinhvien")
public class SinhVien extends Model<SinhVien> implements Serializable{
private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "ten")
	private String ten;

	@Column(name = "email")
	private String email;

	@Column(name = "sdt")
	private String sdt;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "phongtro_id", nullable = true)
	private PhongTro phongTro;
	
	@Column(name = "trangthai")
	private String trangthai;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy="sv")
	private TaiKhoan tk;
	
	public SinhVien() {
		trangthai = "apdung";
	}
	
	public SinhVien(String ten, String email, String sdt, PhongTro phongTro, String trangthai, TaiKhoan tk) {
		super();
		this.ten = ten;
		this.email = email;
		this.sdt = sdt;
		this.phongTro = phongTro;
		this.trangthai = trangthai;
		this.tk = tk;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public PhongTro getPhongTro() {
		return phongTro;
	}

	public void setPhongTro(PhongTro phongTro) {
		this.phongTro = phongTro;
	}

	public String getTrangthai() {
		return trangthai;
	}



	public void setTrangthai(String trangthai) {
		this.trangthai = trangthai;
	}



	public TaiKhoan getTk() {
		return tk;
	}

	public void setTk(TaiKhoan tk) {
		this.tk = tk;
	}

}
