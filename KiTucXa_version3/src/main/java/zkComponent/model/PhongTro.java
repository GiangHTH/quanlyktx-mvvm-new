/**
 * 
 */
package zkComponent.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;


/**
 * @author PC
 *
 */
@Entity
@Table(name = "phongtro")
public class PhongTro extends Model<PhongTro> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;

	@Column(name = "ten")
	private String ten;
	
	@Column(name = "trangthai")
	private String trangthai;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "phongTro")
	private Set<SinhVien> listSV = new HashSet<SinhVien>();

	public PhongTro() {
		trangthai = "apdung";
	}
	
	public PhongTro(String ten, String trangthai) {
		this.ten = ten;
		this.trangthai = trangthai;
	}
	
	public PhongTro(String ten, String trangthai, Set<SinhVien> listSV) {
		this.ten = ten;
		this.trangthai = trangthai;
		this.listSV = listSV;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	

	public String getTrangthai() {
		return trangthai;
	}

	public void setTrangthai(String trangthai) {
		this.trangthai = trangthai;
	}

	public Set<SinhVien> getListSV() {
		return listSV;
	}

	public void setListSV(Set<SinhVien> listSV) {
		this.listSV = listSV;
	}
	
	@Command
	public void savePT(@BindingParam("vm") Object vm) {
		System.out.println("ten: " + ten);
		save();
		BindUtils.postNotifyChange(null, null, vm, "listPT"); 
	}

	@Command
	public void deletePT(@BindingParam("vm") Object vm) {
		System.out.println("==========deletePT");
		System.out.println("id: "+id);
		System.out.println("ten: "+ten);
		trangthai = "daxoa";
		System.out.println("vm"+vm);
		update();
		BindUtils.postNotifyChange(null, null, vm, "listPT"); //listPT này sẽ gọi phương thức getListPT bên PhongTroService.java
	}
	
	@Command
	public void editPT() {
		update();
	}
}


 