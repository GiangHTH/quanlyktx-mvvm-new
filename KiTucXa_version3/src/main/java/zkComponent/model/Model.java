/**
 * 
 */
package zkComponent.model;


import javax.persistence.Transient;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zkplus.spring.SpringUtil;

import zkComponent.service.CRUDService;

/**
 * @author PC
 *
 */
public class Model<T extends Model<T>> {
	
	/*@Column(name = "trangthai")
	private String trangthai;*/
	//@Transient
	private CRUDService crudService;
	public CRUDService crudService() {
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		return crudService;
	}

	public void save() {
		crudService().save(Model.this);
	}

	public void delete() {
		crudService().delete(Model.this);
	}
	
	public void update(){
		crudService().update(Model.this);
	}
	
}
