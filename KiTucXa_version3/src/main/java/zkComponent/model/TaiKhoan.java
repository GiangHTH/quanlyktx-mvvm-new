/**
 * 
 */
package zkComponent.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


/**
 * @author PC
 *
 */
@Entity
@Table(name = "taikhoan")
public class TaiKhoan extends Model<TaiKhoan> implements Serializable {
private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@Column(name = "trangthai")
	private String trangthai;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private SinhVien sv;
	
	public TaiKhoan() {
	}
	
	public TaiKhoan(String username, String password, String trangthai, SinhVien sv) {
		super();
		this.username = username;
		this.password = password;
		this.trangthai = trangthai;
		this.sv = sv;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getTrangthai() {
		return trangthai;
	}



	public void setTrangthai(String trangthai) {
		this.trangthai = trangthai;
	}



	public SinhVien getSv() {
		return sv;
	}
	public void setSv(SinhVien sv) {
		this.sv = sv;
	}

	
}
