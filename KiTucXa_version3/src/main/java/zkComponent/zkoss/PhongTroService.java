/**
 * 
 */
package zkComponent.zkoss;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;

import zkComponent.model.PhongTro;
import zkComponent.service.CRUDService;

/**
 * @author PC
 *
 */
public class PhongTroService {
	@WireVariable
	private CRUDService crudService;

	private String keyword;

	private String id_phongtro;
	private String ten_phongtro;

	private PhongTro newPT = new PhongTro();

	private PhongTro selectPt;

	private List<PhongTro> listPT = null;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getId_phongtro() {
		return id_phongtro;
	}

	public void setId_phongtro(String id_phongtro) {
		this.id_phongtro = id_phongtro;
	}

	public String getTen_phongtro() {
		return ten_phongtro;
	}

	public void setTen_phongtro(String ten_phongtro) {
		this.ten_phongtro = ten_phongtro;
	}

	public PhongTro getNewPT() {
		return newPT;
	}

	public void setNewPT(PhongTro newPT) {
		this.newPT = newPT;
	}

	public PhongTro getSelectPt() {
		return selectPt;
	}

	public void setSelectPt(PhongTro selectPt) {
		this.selectPt = selectPt;
	}

	public List<PhongTro> getListPT() {
		if (!flag) {
			listPT = crudService.getAll(PhongTro.class);
		}
		return listPT;
	} // BindUtils.postNotifyChange(null, null, vm, "listPT"); bên PhongTro.java có listPT sẽ gọi đến phương thức này
	 // nên ta phải map cho listPT một giá trị, nếu không ta sẽ không bind được dữ liệu

	public void setListPT(List<PhongTro> listPT) {
		this.listPT = listPT;
	}

	@Init
	public void initSetup() {
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		listPT = crudService.getAll(PhongTro.class);
	}

	boolean flag = false;
	
	@Command
	@NotifyChange("listPT")
	public void SearchPT() {
		flag = true;
		listPT = crudService.searchPT(keyword);
	}
}
