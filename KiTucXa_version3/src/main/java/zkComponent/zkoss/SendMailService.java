/**
 * 
 */
package zkComponent.zkoss;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import zkComponent.mail.SendMail;
import zkComponent.model.SinhVien;
import zkComponent.service.CRUDService;

/**
 * @author PC
 *
 */
public class SendMailService {
	@Wire("#mail")
	private Window win;
	
	@WireVariable
	private SendMail sendMail;
	
	@WireVariable
	private CRUDService crudService;
	
	//chủ đề mail
	private String subject;
	
	//nội dung mail
	private String mgs;
	
	//danh sách tất cả các mail có trong CSDL
	private List<SinhVien> listMailSV = null;
	
	//danh sách email người nhận
	private List<String> listSelectedMail;
	
	//email người nhận
	private SinhVien sinhvien;

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMgs() {
		return mgs;
	}
	public void setMgs(String mgs) {
		this.mgs = mgs;
	}
	
	public List<SinhVien> getListMailSV() {
		return listMailSV;
	}
	public void setListMailSV(List<SinhVien> listMailSV) {
		this.listMailSV = listMailSV;
	}
	
	public List<String> getListSelectedMail() {
		return listSelectedMail;
	}
	public void setListSelectedMail(List<String> listSelectedMail) {
		this.listSelectedMail = listSelectedMail;
	}
	public SinhVien getSinhvien() {
		return sinhvien;
	}
	public void setSinhvien(SinhVien sinhvien) {
		this.sinhvien = sinhvien;
	}
	
	//list ra danh sách email của tất cả sinh viên
	@AfterCompose
	public void initSetup(){
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		listMailSV = crudService.getAll(SinhVien.class);
		listSelectedMail = new ArrayList<>();
		
	}
	
	//mỗi lần chọn mail từ combobox sẽ
	//gán email đó ( email người nhận) vào danh sách email người nhận
	@Command
	@NotifyChange("listSelectedMail")
	public void selectEmail(){
		System.out.println("email: "+sinhvien.getEmail());
		listSelectedMail.add(sinhvien.getEmail());
	}
	
	@Command
	@NotifyChange("listSelectedMail")
	public void delEmail(@BindingParam("del") String del){
		listSelectedMail.remove(del);
	}
	
	
	@Command
	public void send(){
		sendMail = (SendMail) SpringUtil.getBean("sendMail");
		try {
			sendMail.sendMail("gianggiang200196@gmail.com", listSelectedMail.toArray(new String[0]), subject, mgs);
			win.detach();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		
	}
}
